from django.shortcuts import render

# Create your views here.
response = {}
def index(request):
    response['author'] = 'Nina Nursita Ramadhan'
    return render(request, "lab_8/lab_8.html", response)
