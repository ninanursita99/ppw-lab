window.fbAsyncInit = function() {
  FB.init({
    appId      : '132982940699063',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  FB.AppEvents.logPageView();

  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      console.log(response.status);
      // the user is logged in and has authenticated your
      // app, and response.authResponse supplies
      // the user's ID, a valid access token, a signed
      // request, and the time the access token
      // and signed request each expire
      var uid = response.authResponse.userID;
      var accessToken = response.authResponse.accessToken;
    } else if (response.status === 'not_authorized') {
      // the user is logged in to Facebook,
      // but has not authenticated your app
      console.log('not author')
    } else {
      // the user isn't logged in to Facebook.
      console.log('Not Login')

    }
   });

};


(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.11&appId=1999209187016695';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = loginFlag => {
  if (loginFlag) {
    // Jika yang akan dirender adalah tampilan sudah login

    // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#lab_8').html(
        '<div class="profile">' +
          '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
          '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
          '<div class="data">' +
            '<h1>' + user.name + '</h1>' +
            '<h2>' + user.about + '</h2>' +
            '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
          '</div>' +
        '</div>' +
        '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
        '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
        '<button class="logout" onclick="facebookLogout()">Logout</button>'
      );

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story) {
            $('#lab8').append(
              '<div class="feed">' +
                '<h1>' + value.message + '</h1>' +
                '<h2>' + value.story + '</h2>' +
              '</div>'
            );
          } else if (value.message) {
            $('#lab8').append(
              '<div class="feed">' +
                '<h1>' + value.message + '</h1>' +
              '</div>'
            );
          } else if (value.story) {
            $('#lab8').append(
              '<div class="feed">' +
                '<h2>' + value.story + '</h2>' +
              '</div>'
            );
          }
        });
      });
    });
  }
};

// TODO: Lengkapi Method Ini
// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
// lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di
// method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan
// meneruskan response yang didapat ke fungsi callback tersebut
// Apakah yang dimaksud dengan fungsi callback
// Callback adalah teknik memasangkan beberapa tugas agar berjalan berurutan
//Src Ans: http://armoe.blogspot.co.id/2016/06/apa-itu-callback-asyncronous-dan.html
function getUserData(){
  FB.getLoginStatus(function(response) {
    if(response.status === 'connected'){
      FB.api('/me', 'GET', {fields:'gender, email, about, first_name, last_name, name, id, age_range, verified, picture.width(200).height(200), cover'},function(response){
        console.log(response);
        document.getElementById('userPic').innerHTML= " <a href="+"https://facebook.com/"+response.id+" target="+"_blank"+"><img src='" + response.picture.data.url + "'> </a>";
        document.getElementById('UserData').innerHTML= 'Welcome, ' + response.name + " (ID: " + response.id +')';
        document.getElementById('firstName').innerHTML= response.first_name;
        document.getElementById('lastName').innerHTML= response.last_name;
        document.getElementById('gender').innerHTML= response.gender;
        document.getElementById('age').innerHTML= response.age_range.min;
        document.getElementById('status').innerHTML= response.verified;
        document.getElementById('email').innerHTML= response.email;

        console.log(response.name);
      })
    }
    else {
      document.getElementById('UserData').innerHTML='Login First';
    }
  })
};

const getUserFeed = (fun) => {
  // TODO: Implement Method Ini
  // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
  // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
  // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
  // tersebut
};
function postStatus(){
  FB.ui({
    method: 'share',
    caption: 'Tell us about this website:',
    href: 'https://tes-ppw.herokuapp.com/lab-8/',
}, function(response){
});
};
