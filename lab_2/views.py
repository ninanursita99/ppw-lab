from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'My name is Nina and welcome to my page! I was born in Jakarta, 18 years ago and I am an ESTJ and I know this from MBTI tests. Yay!'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
