from django.conf.urls import url
from .views import index

#https://tes-ppw.herokuapp.com/lab-2

urlpatterns = [
    url(r'^$', index, name='index'),
]
